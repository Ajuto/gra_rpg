#ifndef FUNKCJE_H
#define FUNKCJE_H
#include <string>
using namespace std;
/**
 * \file funkcje.h
 * Plik nagłówkowy modułu funkcje
 */
/**
 * Definicja struktury postać.
 */
struct POSTAC
{ ///Klasa postaci
  int klasa;
  ///Wartość ataku
  int atak;
  ///Wartość obrony
  int obrona;
  ///Wartość magii
  int magia;
  ///Czy postać zatruwa atakując
  bool truj;
  ///Czy postać jest zatruta
  bool jad;
  ///Liczba punktów doświadczenia
  int exp;
  ///Liczba punktów zdrowia
  int hp;
  ///Maksymalna liczba punktów zdrowia
  int hpmax;
  ///Liczba punktów many
  int mana;
  ///Czy postać może użyć zaklęcia leczenia
  bool lecz;
  ///Czy postać może użyć zaklęcia kuli ognia
  bool ogien;
  ///Liczba złota
  int kasa;
  ///Liczba mikstur życia
  int poth;
  ///Liczba mikstur many
  int potm;
  ///Maksymalna liczba punktów zdrowia
  int manamax;
  ///Czy postać może użyć umiejętności furii
  bool furia;
};

/**
 * Definicja struktury potwór
 */
struct POTWOR
{ ///Poziom potwora
  int lvl;
  ///Wartość ataku
  int atak;
  ///Wartość obrony
  double obrona;
  ///Czy potwór zatruwa
  bool truj;
  ///Czy potwór jest otruty
  bool jad;
  ///Liczba punktów życia
  int hp;
  ///Wartość obrony przed magią
  double anty;
};

void tworzenie(POSTAC &hero, string nazwa10);
void loswrog(POTWOR &potwor, int poziom);
void atakhero(POTWOR &potwor, POSTAC hero, int poziom);
void statywrog(POTWOR potwor);
void atakwrog(POTWOR potwor, POSTAC &hero, int poziom);
void wyswietl(POSTAC hero, int poziom);
void zapisanie(POSTAC &hero, string nazwa5);
void wczytanie(POSTAC &hero, string nazwa4);
void atakhero2g(POSTAC &hero1, POSTAC hero, int poziom);

#endif // FUNKCJE_H
