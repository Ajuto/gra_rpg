#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <unistd.h>
#include <string>
#include "Itemy.h"
#include "funkcje.h"
using namespace std;
/**
 * \mainpage
 * \par Turowa gra rpg
 * \author Aleksander Darka
*/
/**
 * \brief aktualizuj
 * Funkcja aktualizująca statystyki gracza
 * \param skad wskaźnik na listę jednokierunkową ekwipunku
 * \param ileitem liczba przedmiotów w ekwipunku
 * \param hero statystyki gracza
 */
void aktualizuj(ITEM *skad, int ileitem, POSTAC &hero)
{ for(int i=0;i<ileitem;i++)
  { if(skad->uzywany==1)
    { hero.atak+=skad->atak;
      hero.obrona-=skad->obrona;
      hero.magia+=skad->magia;
    }
    skad=skad->next;
  }
}

int main()
{ POSTAC hero;
  string nazwa;
  string nazwa12;
  string nazwa24;
  string nazwa2g;
  POTWOR potwor;
  ///poziom gracza
  int poziom=1;
  int poziom2;
  ///Liczba przedmotów w ekwipunku
  int ileitem=0;
  int ileitem2;
  ///Liczba używanych przedmiotów
  int zalozony=0;
  int zalozony2;
  ///Liczba przedmiotów w sklepie
  int ilesklep=0;
  int ilesklep2;
  ofstream zapis("zapis.txt");
  zapis<<ileitem<<" "<<poziom<<" "<<" "<<ilesklep<<" "<<zalozony;
  zapis.close();
  ifstream odczyt("zapis.txt");
  odczyt>>ileitem>>poziom>>ilesklep>>zalozony;
  odczyt.close();
  ///Wskaźnik na początek listy przedmiotów
  ITEM *przedmiot=NULL;
  ///Wskaźnik na początek listy ekwipunku
  ITEM *glowa=NULL;
  ///Wskaźnik na początek listy sklepu
  ITEM *sklep=NULL;
  ITEM *glowa2=NULL;
  ITEM *sklep2=NULL;
  srand(time(NULL));
  /*
  cout<<"                 Gra_rpg";
  sleep(2);
  system("cls");
  cout<<"                 Ajuto game studio";
  sleep(2);
  cout<<"\n\n                    prezentuje:";
  sleep(2);
  system("cls");
  cout<<"\n\n                     Ain't no rest";
  sleep(3);
  system("cls");
  */
  ///zmienna służąca do wyboru opcji w menu
  char temp;
  do
  { menuglowne:
    cout<<"\nWybierz opcje. Wpisz 'n' dla nowej gry,'p' dla przejscia w tryb pvp, 'w' dla wczytania gry lub 'q' zeby wyjsc:";
    cin>>temp;
  }
  while(temp!='n'&&temp!='w'&&temp!='q'&&temp!='p');
  switch(temp)
  { case 'n':
    { system("cls");
      tworzenie(hero, nazwa12);
      cout<<"\nPodaj nazwe pliku do zapisu:";
      cin>>nazwa12;
      nazwa=nazwa12+".txt";
      zapisanie(hero,nazwa12);
      poziom=1;
      ileitem=0;
      sklep=losujsklep(poziom);
      ilesklep=poziom;
      ofstream zapis(nazwa.c_str());
      zapis<<ileitem<<" "<<poziom<<" "<<ilesklep<<" "<<zalozony;
      zapis.close();
      ofstream oplik(nazwa.c_str());
      oplik.close();
      nazwa=nazwa12+"sklep.txt";
      ofstream osklep(nazwa.c_str());
      osklep.close();
      nazwa=nazwa12+"zapis.txt";
      ifstream odczyt(nazwa.c_str());
      odczyt>>ileitem>>poziom>>ilesklep>>zalozony;
      odczyt.close();
      przedmiot=tworz();
      usunliste(glowa);
      system("cls");
      wyswietl(hero, poziom);
      break;
    }
    case 'p':
    { system("cls");
      POSTAC gracz1;
      POSTAC gracz2;
      cout<<"\nPodaj nazwe pliku do wczytania: ";
      cin>>nazwa12;
      nazwa=nazwa12+".txt";
      system("cls");
      wczytanie(gracz1, nazwa12);
      ifstream odczyt7(nazwa.c_str());
      odczyt7>>ileitem>>poziom>>ilesklep>>zalozony;
      odczyt7.close();
      wczytajekw(ileitem, glowa, nazwa12);
      wczytajsklep(ilesklep, sklep, nazwa12);
      aktualizuj(glowa, ileitem, gracz1);
      cout<<"\nPodaj nazwe 2. pliku do wczytania: ";
      cin>>nazwa24;
      nazwa2g=nazwa24+".txt";
      system("cls");
      wczytanie(gracz2,nazwa24);
      ifstream odczyt8(nazwa2g.c_str());
      odczyt8>>ileitem2>>poziom2>>ilesklep2>>zalozony2;
      odczyt8.close();
      wczytajekw(ileitem2, glowa2, nazwa24);
      wczytajsklep(ilesklep2, sklep2, nazwa24);
      aktualizuj(glowa2, ileitem2, gracz2);
      przedmiot=tworz();
      tura1g:
      char z2g;
      do
      { system("cls");
        cout<<"\nTura pierwszego gracza.";
        wyswietl(gracz1, poziom);
        cout<<"\nWybierz co chcesz robic. Wpisz:\n'w', aby walczyc\n'm', aby pojsc do miasta\n'q', aby wyjsc\n";
        cin>>z2g;
      }
      while(z2g!='w'&&z2g!='q'&&z2g!='m');
      switch(z2g)
      { case 'w':
        { menuwalka1g:
          { do
            { system("cls");
              wyswietl(gracz1, poziom);
              cout<<"\nStatystyki przeciwnika:\n";
              wyswietl(gracz2, poziom2);
              if(gracz1.klasa==0) cout<<"\nWybierz co chcesz robic. Wpisz:\n'a', aby zaatkowac\n'm', aby uzyc magii\n'p', aby uzyc mikstury\n";
              if(gracz1.klasa==1) cout<<"\nWybierz co chcesz robic. Wpisz:\n'a', aby zaatkowac\n'm', aby uzyc umiejetnosci\n'p', aby uzyc mikstury\n";
              cin>>temp;
            }
            while(temp!='a'&&temp!='m'&&temp!='p');
            switch(temp)
            { case 'a':
              { atakhero2g(gracz2,gracz1,poziom);
                if(gracz1.hp<=0)
                { cout<<"\nWrog zostal pokonany!";
                  sleep(1);
                  goto menuglowne;
                }
                else goto tura2g;
                }
              case 'm':
              { if(gracz1.klasa==0)
                { do
                  { menumagia1g:
                    system("cls");
                    wyswietl(gracz1, poziom);
                    cout<<"\nStatystyki przeciwnika:\n";
                    wyswietl(gracz2, poziom2);
                    cout<<"\nWybierz rodzaj czaru. Wpisz:\n'k' dla kuli ognia\n'l' dla leczenia\n'q', aby wrocic\n";
                    cin>>temp;
                  }
                  while(temp!='k'&&temp!='l'&&temp!='q');
                  switch(temp)
                  { case 'k':
                    { if(gracz1.ogien)
                      { gracz1.mana-=50;
                        if(gracz1.mana<0)
                        { gracz1.mana+=50;
                          system("cls");
                          cout<<"\nNie masz tyle many.\n";
                          sleep(2);
                          goto menumagia1g;
                        }
                        gracz2.hp-=3*gracz1.magia;
                        goto pomagii1g;
                      }
                      else
                      { system("cls");
                        cout<<"\nNie znasz jeszcze tego czaru.\n";
                        sleep(2);
                        goto menumagia1g;
                      }
                    }
                    case 'l':
                    { if(gracz1.lecz)
                      { gracz1.mana-=50;
                        if(gracz1.mana<0)
                        { gracz1.mana+=50;
                          system("cls");
                          cout<<"\nNie masz tyle many.\n";
                          sleep(2);
                          goto menumagia1g;
                        }
                        int hptemp1g=gracz1.hp+8*gracz1.magia;
                        if(hptemp1g>gracz1.hpmax) gracz1.hp=gracz1.hpmax;
                        else gracz1.hp+=8*gracz1.magia;
                        goto pomagii1g;
                      }
                      else
                      { system("cls");
                        cout<<"\nNie znasz jeszcze tego czaru.\n";
                        sleep(2);
                        goto menumagia1g;
                      }
                    }
                    case 'q':
                    { goto menuwalka1g;
                    }
                  }
                }
                if(gracz1.klasa==1)
                { do
                  { menufuria1g:
                    system("cls");
                    wyswietl(gracz1, poziom);
                    drukuj(glowa);
                    cout<<"\nStatystyki przeciwnika:\n";
                    wyswietl(gracz2, poziom2);
                    cout<<"\nWpisz:\n'f', aby uzyc furie\n'q', aby wrocic\n";
                    cin>>temp;
                  }
                  while(temp!='f'&&temp!='q');
                  switch(temp)
                  { case 'f':
                    { if(gracz1.furia)
                      { gracz1.mana-=60;
                        if(gracz1.mana<0)
                        { gracz1.mana+=50;
                          system("cls");
                          cout<<"\nNie masz tyle many.\n";
                          sleep(2);
                          goto menufuria1g;
                       }
                       gracz2.hp-=2*gracz1.atak;
                       goto pomagii1g;
                      }
                      else
                      { system("cls");
                        cout<<"\nNie znasz jeszcze tej umiejetnosci.\n";
                        sleep(2);
                        goto menufuria1g;
                      }
                    }
                   case 'q':
                   { goto menuwalka1g;
                   }
                 }
               }
             }
             pomagii1g:
             if(gracz1.hp<=0)
             { cout<<"\nWrog zostal pokonany!";
               sleep(1);
               goto menuglowne;
             }
             else goto tura2g;
             case 'p':
             { do
               { menupotion1g:
                 system("cls");
                 wyswietl(gracz1, poziom);
                 cout<<"\nWpisz:\n'h', aby uzyc mikstury zdrowia\n'm', aby uzyc mikstury many\n'q', aby wrocic\n";
                 cin>>temp;
               }
               while(temp!='h'&&temp!='m'&&temp!='q');
               switch(temp)
               { case 'h':
                 { if(gracz1.poth>0)
                   { gracz1.poth-=1;
                     int hptemp1g=gracz1.hp+30*poziom;
                     if(hptemp1g>gracz1.hpmax) gracz1.hp=gracz1.hpmax;
                     else gracz1.hp+=30*poziom;
                     goto menupotion1g;
                   }
                   else
                   { system("cls");
                     cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                     sleep(2);
                     goto menupotion1g;
                   }
                 }
                 case 'm':
                 { if(gracz1.potm>0)
                   { gracz1.potm-=1;
                     int manatemp1g=gracz1.mana+30*poziom;
                     if(manatemp1g>gracz1.manamax) gracz1.mana=gracz1.manamax;
                     else gracz1.mana+=30*poziom;
                     goto menupotion1g;
                   }
                   else
                   { system("cls");
                     cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                     sleep(2);
                     goto menupotion1g;
                   }
                 }
                 case 'q':
                 { goto menuwalka1g;
                 }
               }
             }
           }
           }
         }
       case 'm':
       { menumiasto1g:
         do
         { system("cls");
           cout<<"\nWybierz co chcesz robic. Wpisz:\n'e', aby otworzyc ekwipunek\n's', aby pojsc do sklepu\n'q', aby wrocic\n";
           cin>>temp;
         }
         while(temp!='e'&&temp!='s'&&temp!='q');
         switch(temp)
         { case 'e':
           { menuekwipunek1g:
             system("cls");
             cout<<"\nOto Twoj ekwipunek: \n";
             drukuj(glowa);
             do
             { cout<<"\nWpisz:\n'p', aby zalozyc przedmiot\n't', aby zdjac przedmiot\n'm', aby uzyc miksury\n'q', aby wrocic\n";
               cin>>temp;
             }
             while(temp!='p'&&temp!='t'&&temp!='q'&&temp!='m');
             switch(temp)
             { case 'p':
              { char nrp;
                 cout<<"\nPodaj nr przedmiotu, ktory chcesz zalozyc: \n";
                 cin>>nrp;
                 int nrp2=static_cast<int>(nrp)-48;
                 if((ileitem==0)||(nrp2>ileitem))
                 { system("cls");
                   cout<<"\nNie ma takiego przedmiotu.\n";
                   sleep(2);
                   goto menuekwipunek1g;
                 }
                 else
                 { przedmiot=tworz();
                   przedmiot=znajdz(glowa, nrp2);
                   if((przedmiot->uzywany==0)&&(zalozony==poziom))
                   { system("cls");
                     cout<<"\nNie mozesz niesc wiecej przedmiotow.\n";
                     sleep(2);
                     goto menuekwipunek1g;
                   }
                   else
                   { przedmiot->uzywany=1;
                     zalozony+=1;
                     aktualizuj(glowa,ileitem,gracz1);
                     goto menuekwipunek1g;
                  }
                 }
               }
               case 't':
               { char z;
                 cout<<"\nPodaj nr przedmiotu, ktory chcesz zdjac: \n";
                 cin>>z;
                 int nrk=static_cast<int>(z)-48;
                 if((ileitem==0)||(nrk>ileitem))
                 { system("cls");
                   cout<<"\nNie ma takiego przedmiotu.\n";
                   sleep(2);
                   goto menuekwipunek1g;
                 }
                 else
                 { przedmiot=tworz();
                   przedmiot=znajdz(glowa, nrk);
                   if(przedmiot->uzywany==0) goto menuekwipunek1g;
                   else
                   { przedmiot->uzywany=0;
                     zalozony-=1;
                     aktualizuj(glowa,ileitem,gracz1);
                     goto menuekwipunek1g;
                   }
                 }
               }
               case 'm':
               { do
                 { menupotionsklep1g:
                   system("cls");
                   wyswietl(gracz1, poziom);
                   cout<<"\nWpisz:\n'h', aby uzyc mikstury zdrowia\n'm', aby uzyc mikstury many\n'q', aby wrocic\n";
                   cin>>temp;
                 }
                 while(temp!='h'&&temp!='m'&&temp!='q');
                 switch(temp)
                 { case 'h':
                  { if(gracz1.poth>0)
                     { gracz1.poth-=1;
                       int hptemp=gracz1.hp+30*poziom;
                       if(hptemp>gracz1.hpmax) gracz1.hp=gracz1.hpmax;
                       else gracz1.hp+=30*poziom;
                       goto menupotionsklep1g;
                     }
                     else
                     { system("cls");
                       cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                       sleep(2);
                       goto menupotionsklep1g;
                    }
                  }
                  case 'm':
                  { if(gracz1.potm>0)
                    { gracz1.potm-=1;
                      int manatemp=gracz1.mana+30*poziom;
                      if(manatemp>gracz1.manamax) gracz1.mana=gracz1.manamax;
                      else gracz1.mana+=30*poziom;
                      goto menupotionsklep1g;
                    }
                    else
                    { system("cls");
                      cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                      sleep(2);
                      goto menupotionsklep1g;
                    }
                  }
                  case 'q':
                  { goto menuekwipunek1g;
                  }
                }
              }
              case 'q':
              { goto menumiasto1g;
              }
            }
          }
          case 's':
          { menusklep1g:
            char z2;
            do
            { system("cls");
              cout<<"\nTwoja ilosc zlota:     "<<gracz1.kasa<<"\n";
              cout<<"\nWpisz:\n's', aby sprzedac przedmiot\n'k', aby kupic przedmiot\n'm',aby kupic miksture\n'q' aby wrocic \n";
              cin>>z2;
            }
            while(z2!='s'&&z2!='k'&&z2!='q'&&z2!='m');
            switch(z2)
            { case 's':
              { char x;
                system("cls");
                cout<<"\nOto Twoj ekwipunek: \n";
                drukuj(glowa);
                cout<<"\nTwoja ilosc zlota:     "<<gracz1.kasa<<"\n";
                cout<<"\nPodaj nr przedmiotu, ktory chcesz sprzedac: \n";
                cin>>x;
                int nrs=static_cast<int>(x)-48;
                if((nrs<=0)||(nrs>ileitem))
                { system("cls");
                  cout<<"\nNie ma takiego przedmiotu.\n";
                  sleep(2);
                  goto menusklep1g;
                }
                else
                { przedmiot=tworz();
                  przedmiot=znajdz(glowa,nrs);
                  gracz1.kasa+=przedmiot->wart;
                  if(przedmiot->uzywany==1) zalozony-=1;
                  usunelem(glowa,przedmiot);
                  ileitem-=1;
                  goto menusklep1g;
                }
              }
              case 'k':
              { menukupno1g:
                system("cls");
                cout<<"\nTwoja ilosc zlota:     "<<gracz1.kasa<<"\n";
                cout<<"\nPrzedmioty w sklepie: \n";
                drukuj(sklep);
                char y;
                cout<<"\nPodaj nr przedmiotu, ktory chcesz kupic, jesli nie chcesz nacisnij 0: \n";
                cin>>y;
                int nrk=static_cast<int>(y)-48;
                if((nrk<=0)||(nrk>ilesklep))
                { system("cls");
                  if(nrk!=0)
                  { cout<<"\nNie ma takiego przedmiotu.\n";
                    sleep(2);
                  }
                  goto menusklep1g;
                }
                else
                { przedmiot=tworz();
                  przedmiot=znajdz(sklep,nrk);
                  if(przedmiot->wart<=gracz1.kasa)
                  { gracz1.kasa-=przedmiot->wart;
                    ilesklep-=1;
                    if(ileitem==0)
                    { usunelem(sklep,przedmiot);
                      przedmiot->next=NULL;
                      glowa=przedmiot;
                      ileitem+=1;
                    }
                    else
                    { usunelem(sklep,przedmiot);
                      przedmiot->next=NULL;
                      dodaj(glowa,przedmiot);
                      ileitem+=1;
                    }
                    goto menukupno1g;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie masz wystarczajacej ilosci pieniedzy.\n";
                    sleep(2);
                    goto menukupno1g;
                  }
                }
              }
              case 'm':
              { do
                { menupotionkup1g:
                  system("cls");
                  wyswietl(gracz1, poziom);
                  cout<<"\nWpisz:\n'h', aby kupic miksture zdrowia\n'm', aby kupic miksture many\n'q', aby wrocic\n";
                  cin>>temp;
                }
                while(temp!='h'&&temp!='m'&&temp!='q');
                switch(temp)
                { case 'h':
                  { if(gracz1.kasa-100*poziom>=0)
                    { gracz1.poth+=1;
                      gracz1.kasa-=100*poziom;
                      goto menupotionkup1g;
                    }
                    else
                    { system("cls");
                      cout<<"\nNie masz wystarczajacej ilosci zlota\n";
                      sleep(2);
                      goto menupotionkup1g;
                    }
                  }
                  case 'm':
                  { if(gracz1.kasa-100*poziom>=0)
                    { gracz1.potm+=1;
                      gracz1.kasa-=100*poziom;
                      goto menupotionkup1g;
                    }
                    else
                    { system("cls");
                      cout<<"\nNie masz wystarczajacej ilosci zlota\n";
                      sleep(2);
                      goto menupotionkup1g;
                    }
                  }
                  case 'q':
                  { goto menusklep1g;
                  }
                }
              }
              case 'q':
              { goto menumiasto1g;
              }
            }
          }
          case 'q':
          { goto tura1g;
          }
        }
      }
      case 'q':
      { goto menuglowne;
        system("cls");
      }
    }





tura2g:
do
{ system("cls");
  cout<<"\nTura pierwszego gracza.";
  wyswietl(gracz2, poziom2);
  cout<<"\nWybierz co chcesz robic. Wpisz:\n'w', aby walczyc\n'm', aby pojsc do miasta\n'q', aby wyjsc\n";
  cin>>z2g;
}
while(z2g!='w'&&z2g!='q'&&z2g!='m');
switch(z2g)
{ case 'w':
  { menuwalka2g:
    { do
      { system("cls");
        wyswietl(gracz2, poziom2);
        cout<<"\nStatystyki przeciwnika:\n";
        wyswietl(gracz1, poziom);
        if(gracz2.klasa==0) cout<<"\nWybierz co chcesz robic. Wpisz:\n'a', aby zaatkowac\n'm', aby uzyc magii\n'p', aby uzyc mikstury\n";
        if(gracz2.klasa==1) cout<<"\nWybierz co chcesz robic. Wpisz:\n'a', aby zaatkowac\n'm', aby uzyc umiejetnosci\n'p', aby uzyc mikstury\n";
        cin>>temp;
      }
      while(temp!='a'&&temp!='m'&&temp!='p');
      switch(temp)
      { case 'a':
        { atakhero2g(gracz1,gracz2,poziom2);
          if(gracz2.hp<=0)
          { cout<<"\nWrog zostal pokonany!";
            sleep(1);
            goto menuglowne;
          }
          else goto tura1g;
          }
        case 'm':
        { if(gracz2.klasa==0)
          { do
            { menumagia2g:
              system("cls");
              wyswietl(gracz2, poziom2);
              cout<<"\nStatystyki przeciwnika:\n";
              wyswietl(gracz1, poziom);
              cout<<"\nWybierz rodzaj czaru. Wpisz:\n'k' dla kuli ognia\n'l' dla leczenia\n'q', aby wrocic\n";
              cin>>temp;
            }
            while(temp!='k'&&temp!='l'&&temp!='q');
            switch(temp)
            { case 'k':
              { if(gracz2.ogien)
                { gracz2.mana-=50;
                  if(gracz2.mana<0)
                  { gracz2.mana+=50;
                    system("cls");
                    cout<<"\nNie masz tyle many.\n";
                    sleep(2);
                    goto menumagia2g;
                  }
                  gracz1.hp-=3*gracz2.magia;
                  goto pomagii2g;
                }
                else
                { system("cls");
                  cout<<"\nNie znasz jeszcze tego czaru.\n";
                  sleep(2);
                  goto menumagia2g;
                }
              }
              case 'l':
              { if(gracz2.lecz)
                { gracz2.mana-=50;
                  if(gracz2.mana<0)
                  { gracz2.mana+=50;
                    system("cls");
                    cout<<"\nNie masz tyle many.\n";
                    sleep(2);
                    goto menumagia2g;
                  }
                  int hptemp2g=gracz2.hp+8*gracz2.magia;
                  if(hptemp2g>gracz2.hpmax) gracz2.hp=gracz2.hpmax;
                  else gracz2.hp+=8*gracz2.magia;
                  goto pomagii2g;
                }
                else
                { system("cls");
                  cout<<"\nNie znasz jeszcze tego czaru.\n";
                  sleep(2);
                  goto menumagia2g;
                }
              }
              case 'q':
              { goto menuwalka2g;
              }
            }
          }
          if(gracz2.klasa==1)
          { do
            { menufuria2g:
              system("cls");
              wyswietl(gracz2, poziom2);
              cout<<"\nStatystyki przeciwnika:\n";
              wyswietl(gracz1, poziom);
              cout<<"\nWpisz:\n'f', aby uzyc furie\n'q', aby wrocic\n";
              cin>>temp;
            }
            while(temp!='f'&&temp!='q');
            switch(temp)
            { case 'f':
              { if(gracz2.furia)
                { gracz2.mana-=60;
                  if(gracz2.mana<0)
                  { gracz2.mana+=50;
                    system("cls");
                    cout<<"\nNie masz tyle many.\n";
                    sleep(2);
                    goto menufuria2g;
                 }
                 gracz1.hp-=2*gracz2.atak;
                 goto pomagii2g;
                }
                else
                { system("cls");
                  cout<<"\nNie znasz jeszcze tej umiejetnosci.\n";
                  sleep(2);
                  goto menufuria2g;
                }
              }
             case 'q':
             { goto menuwalka2g;
             }
           }
         }
       }
       pomagii2g:
       if(gracz2.hp<=0)
       { cout<<"\nWrog zostal pokonany!";
         sleep(1);
         goto menuglowne;
       }
       else goto tura1g;
       case 'p':
       { do
         { menupotion2g:
           system("cls");
           wyswietl(gracz2, poziom2);
           cout<<"\nWpisz:\n'h', aby uzyc mikstury zdrowia\n'm', aby uzyc mikstury many\n'q', aby wrocic\n";
           cin>>temp;
         }
         while(temp!='h'&&temp!='m'&&temp!='q');
         switch(temp)
         { case 'h':
           { if(gracz2.poth>0)
             { gracz2.poth-=1;
               int hptemp2g=gracz2.hp+30*poziom2;
               if(hptemp2g>gracz2.hpmax) gracz2.hp=gracz2.hpmax;
               else gracz1.hp+=30*poziom2;
               goto menupotion2g;
             }
             else
             { system("cls");
               cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
               sleep(2);
               goto menupotion2g;
             }
           }
           case 'm':
           { if(gracz2.potm>0)
             { gracz2.potm-=1;
               int manatemp2g=gracz2.mana+30*poziom2;
               if(manatemp2g>gracz2.manamax) gracz2.mana=gracz2.manamax;
               else gracz2.mana+=30*poziom2;
               goto menupotion2g;
             }
             else
             { system("cls");
               cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
               sleep(2);
               goto menupotion2g;
             }
           }
           case 'q':
           { goto menuwalka2g;
           }
         }
       }
      }
     }
   }
 case 'm':
 { menumiasto2g:
   do
   { system("cls");
     cout<<"\nWybierz co chcesz robic. Wpisz:\n'e', aby otworzyc ekwipunek\n's', aby pojsc do sklepu\n'q', aby wrocic\n";
     cin>>temp;
   }
   while(temp!='e'&&temp!='s'&&temp!='q');
   switch(temp)
   { case 'e':
     { menuekwipunek2g:
       system("cls");
       cout<<"\nOto Twoj ekwipunek: \n";
       drukuj(glowa2);
       do
       { cout<<"\nWpisz:\n'p', aby zalozyc przedmiot\n't', aby zdjac przedmiot\n'm', aby uzyc miksury\n'q', aby wrocic\n";
         cin>>temp;
       }
       while(temp!='p'&&temp!='t'&&temp!='q'&&temp!='m');
       switch(temp)
       { case 'p':
        { char nrp;
           cout<<"\nPodaj nr przedmiotu, ktory chcesz zalozyc: \n";
           cin>>nrp;
           int nrp2=static_cast<int>(nrp)-48;
           if((ileitem2==0)||(nrp2>ileitem2))
           { system("cls");
             cout<<"\nNie ma takiego przedmiotu.\n";
             sleep(2);
             goto menuekwipunek2g;
           }
           else
           { przedmiot=tworz();
             przedmiot=znajdz(glowa2, nrp2);
             if((przedmiot->uzywany==0)&&(zalozony2==poziom2))
             { system("cls");
               cout<<"\nNie mozesz niesc wiecej przedmiotow.\n";
               sleep(2);
               goto menuekwipunek2g;
             }
             else
             { przedmiot->uzywany=1;
               zalozony2+=1;
               aktualizuj(glowa2,ileitem2,gracz2);
               goto menuekwipunek2g;
            }
           }
         }
         case 't':
         { char z;
           cout<<"\nPodaj nr przedmiotu, ktory chcesz zdjac: \n";
           cin>>z;
           int nrk=static_cast<int>(z)-48;
           if((ileitem2==0)||(nrk>ileitem2))
           { system("cls");
             cout<<"\nNie ma takiego przedmiotu.\n";
             sleep(2);
             goto menuekwipunek2g;
           }
           else
           { przedmiot=tworz();
             przedmiot=znajdz(glowa2, nrk);
             if(przedmiot->uzywany==0) goto menuekwipunek2g;
             else
             { przedmiot->uzywany=0;
               zalozony2-=1;
               aktualizuj(glowa2,ileitem2,gracz2);
               goto menuekwipunek2g;
             }
           }
         }
         case 'm':
         { do
           { menupotionsklep2g:
             system("cls");
             wyswietl(gracz2, poziom2);
             cout<<"\nWpisz:\n'h', aby uzyc mikstury zdrowia\n'm', aby uzyc mikstury many\n'q', aby wrocic\n";
             cin>>temp;
           }
           while(temp!='h'&&temp!='m'&&temp!='q');
           switch(temp)
           { case 'h':
            { if(gracz2.poth>0)
               { gracz2.poth-=1;
                 int hptemp=gracz2.hp+30*poziom2;
                 if(hptemp>gracz2.hpmax) gracz2.hp=gracz2.hpmax;
                 else gracz2.hp+=30*poziom2;
                 goto menupotionsklep2g;
               }
               else
               { system("cls");
                 cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                 sleep(2);
                 goto menupotionsklep2g;
              }
            }
            case 'm':
            { if(gracz2.potm>0)
              { gracz2.potm-=1;
                int manatemp=gracz2.mana+30*poziom2;
                if(manatemp>gracz2.manamax) gracz2.mana=gracz2.manamax;
                else gracz2.mana+=30*poziom2;
                goto menupotionsklep2g;
              }
              else
              { system("cls");
                cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                sleep(2);
                goto menupotionsklep2g;
              }
            }
            case 'q':
            { goto menuekwipunek2g;
            }
          }
        }
        case 'q':
        { goto menumiasto2g;
        }
      }
    }
    case 's':
    { menusklep2g:
      char z2;
      do
      { system("cls");
        cout<<"\nTwoja ilosc zlota:     "<<gracz2.kasa<<"\n";
        cout<<"\nWpisz:\n's', aby sprzedac przedmiot\n'k', aby kupic przedmiot\n'm',aby kupic miksture\n'q' aby wrocic \n";
        cin>>z2;
      }
      while(z2!='s'&&z2!='k'&&z2!='q'&&z2!='m');
      switch(z2)
      { case 's':
        { char x;
          system("cls");
          cout<<"\nOto Twoj ekwipunek: \n";
          drukuj(glowa2);
          cout<<"\nTwoja ilosc zlota:     "<<gracz2.kasa<<"\n";
          cout<<"\nPodaj nr przedmiotu, ktory chcesz sprzedac: \n";
          cin>>x;
          int nrs=static_cast<int>(x)-48;
          if((nrs<=0)||(nrs>ileitem2))
          { system("cls");
            cout<<"\nNie ma takiego przedmiotu.\n";
            sleep(2);
            goto menusklep2g;
          }
          else
          { przedmiot=tworz();
            przedmiot=znajdz(glowa2,nrs);
            gracz2.kasa+=przedmiot->wart;
            if(przedmiot->uzywany==1) zalozony2-=1;
            usunelem(glowa2,przedmiot);
            ileitem2-=1;
            goto menusklep2g;
          }
        }
        case 'k':
        { menukupno2g:
          system("cls");
          cout<<"\nTwoja ilosc zlota:     "<<gracz2.kasa<<"\n";
          cout<<"\nPrzedmioty w sklepie: \n";
          drukuj(sklep2);
          char y;
          cout<<"\nPodaj nr przedmiotu, ktory chcesz kupic, jesli nie chcesz nacisnij 0: \n";
          cin>>y;
          int nrk=static_cast<int>(y)-48;
          if((nrk<=0)||(nrk>ilesklep2))
          { system("cls");
            if(nrk!=0)
            { cout<<"\nNie ma takiego przedmiotu.\n";
              sleep(2);
            }
            goto menusklep2g;
          }
          else
          { przedmiot=tworz();
            przedmiot=znajdz(sklep2,nrk);
            if(przedmiot->wart<=gracz2.kasa)
            { gracz2.kasa-=przedmiot->wart;
              ilesklep2-=1;
              if(ileitem2==0)
              { usunelem(sklep2,przedmiot);
                przedmiot->next=NULL;
                glowa2=przedmiot;
                ileitem2+=1;
              }
              else
              { usunelem(sklep2,przedmiot);
                przedmiot->next=NULL;
                dodaj(glowa2,przedmiot);
                ileitem2+=1;
              }
              goto menukupno2g;
            }
            else
            { system("cls");
              cout<<"\nNie masz wystarczajacej ilosci pieniedzy.\n";
              sleep(2);
              goto menukupno2g;
            }
          }
        }
        case 'm':
        { do
          { menupotionkup2g:
            system("cls");
            wyswietl(gracz2, poziom2);
            cout<<"\nWpisz:\n'h', aby kupic miksture zdrowia\n'm', aby kupic miksture many\n'q', aby wrocic\n";
            cin>>temp;
          }
          while(temp!='h'&&temp!='m'&&temp!='q');
          switch(temp)
          { case 'h':
            { if(gracz2.kasa-100*poziom2>=0)
              { gracz2.poth+=1;
                gracz2.kasa-=100*poziom2;
                goto menupotionkup2g;
              }
              else
              { system("cls");
                cout<<"\nNie masz wystarczajacej ilosci zlota\n";
                sleep(2);
                goto menupotionkup2g;
              }
            }
            case 'm':
            { if(gracz2.kasa-100*poziom2>=0)
              { gracz2.potm+=1;
                gracz2.kasa-=100*poziom2;
                goto menupotionkup2g;
              }
              else
              { system("cls");
                cout<<"\nNie masz wystarczajacej ilosci zlota\n";
                sleep(2);
                goto menupotionkup2g;
              }
            }
            case 'q':
            { goto menusklep2g;
            }
          }
        }
        case 'q':
        { goto menumiasto2g;
        }
      }
    }
    case 'q':
    { goto tura2g;
    }
  }
}
case 'q':
{ system("cls");
    goto menuglowne;
}
}
  }







    case 'w':
    { system("cls");
      wczytanie(hero,nazwa12);
      cout<<"\nPodaj nazwe pliku do wczytania: ";
      cin>>nazwa12;
      nazwa=nazwa12+".txt";
      ifstream odczyt(nazwa.c_str());
      odczyt>>ileitem>>poziom>>ilesklep>>zalozony;
      odczyt.close();
      wczytajekw(ileitem, glowa,nazwa12);
      wczytajsklep(ilesklep, sklep,nazwa12);
      aktualizuj(glowa, ileitem, hero);
      przedmiot=tworz();
      break;
    }
    case 'q':
    { usunliste(glowa);
      usunliste(przedmiot);
      usunliste(sklep);
      return 0;
    }
  }
  menuakcja:
  char z;
  do
  { system("cls");
    wyswietl(hero, poziom);
    cout<<"\nWybierz co chcesz robic. Wpisz:\n'w', aby walczyc\n'm', aby pojsc do miasta\n'z', aby zapisac\n'q', aby wyjsc\n";
    cin>>z;
  }
  while(z!='w'&&z!='z'&&z!='q'&&z!='m');
  switch(z)
  { case 'w':
    { if(poziom<10) loswrog(potwor,poziom);
      if(poziom==10)
      { potwor.lvl=10;
        potwor.atak=80;
        potwor.obrona=10;
        potwor.truj=1;
        potwor.jad=0;
        potwor.hp=300;
        potwor.anty=10;
      }
      bool wygran=0;
      menuwalka:
      if(wygran==1) sleep(1);
      if(wygran==0)
      { do
        { system("cls");
          wyswietl(hero, poziom);
          statywrog(potwor);
          if(hero.klasa==0) cout<<"\nWybierz co chcesz robic. Wpisz:\n'a', aby zaatkowac\n'm', aby uzyc magii\n'p', aby uzyc mikstury\n";
          if(hero.klasa==1) cout<<"\nWybierz co chcesz robic. Wpisz:\n'a', aby zaatkowac\n'm', aby uzyc umiejetnosci\n'p', aby uzyc mikstury\n";
          cin>>temp;
        }
        while(temp!='a'&&temp!='m'&&temp!='p');
        switch(temp)
        { case 'a':
          { atakhero(potwor,hero,poziom);
            if(potwor.hp<=0)
            { wygran=1;
              cout<<"\nWrog zostal pokonany!";
              sleep(1);
              goto menuwalka;
            }
            atakwrog(potwor,hero,poziom);
            if(hero.hp<=0)
            { wyswietl(hero, poziom);
              sleep(2);
              system("cls");
              cout<<"\nPrzegrales\n";
              sleep(2);
              goto menuglowne;
            }
            goto menuwalka;
          }
          case 'm':
          { if(hero.klasa==0)
            { do
              { menumagia:
                system("cls");
                wyswietl(hero, poziom);
                statywrog(potwor);
                cout<<"\nWybierz rodzaj czaru. Wpisz:\n'k' dla kuli ognia\n'l' dla leczenia\n'q', aby wrocic\n";
                cin>>temp;
              }
              while(temp!='k'&&temp!='l'&&temp!='q');
              switch(temp)
              { case 'k':
                { if(hero.ogien)
                  { hero.mana-=50;
                    if(hero.mana<0)
                    { hero.mana+=50;
                      system("cls");
                      cout<<"\nNie masz tyle many.\n";
                      sleep(2);
                      goto menumagia;
                    }
                    potwor.hp-=5*hero.magia*double(potwor.anty/100);
                    goto pomagii;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie znasz jeszcze tego czaru.\n";
                    sleep(2);
                    goto menumagia;
                  }
                }
                case 'l':
                { if(hero.lecz)
                  { hero.mana-=50;
                    if(hero.mana<0)
                    { hero.mana+=50;
                      system("cls");
                      cout<<"\nNie masz tyle many.\n";
                      sleep(2);
                      goto menumagia;
                    }
                    int hptemp=hero.hp+8*hero.magia;
                    if(hptemp>hero.hpmax) hero.hp=hero.hpmax;
                    else hero.hp+=8*hero.magia;
                    goto pomagii;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie znasz jeszcze tego czaru.\n";
                    sleep(2);
                    goto menumagia;
                  }
                }
                case 'q':
                { goto menuwalka;
                }
              }
            }
            if(hero.klasa==1)
            { do
              { menufuria:
                system("cls");
                wyswietl(hero, poziom);
                drukuj(glowa);
                statywrog(potwor);
                cout<<"\nWpisz:\n'f', aby uzyc furie\n'q', aby wrocic\n";
                cin>>temp;
              }
              while(temp!='f'&&temp!='q');
              switch(temp)
              { case 'f':
                { if(hero.furia)
                  { hero.mana-=60;
                    if(hero.mana<0)
                    { hero.mana+=50;
                      system("cls");
                      cout<<"\nNie masz tyle many.\n";
                      sleep(2);
                      goto menufuria;
                    }
                    potwor.hp-=2*hero.atak*double(potwor.anty/100);
                    goto pomagii;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie znasz jeszcze tej umiejetnosci.\n";
                    sleep(2);
                    goto menufuria;
                  }
                }
                case 'q':
                { goto menuwalka;
                }
              }
            }
          }
          pomagii:
          if(potwor.hp<=0)
          { wygran=1;
            cout<<"\nWrog zostal pokonany!";
            sleep(1);
            goto menuwalka;
          }
          atakwrog(potwor,hero,poziom);
          if(hero.hp<=0)
          { system("cls");
            cout<<"\nPrzegrales\n";
            sleep(2);
            goto menuglowne;
          }
          case 'p':
          { do
            { menupotion:
              system("cls");
              wyswietl(hero, poziom);
              cout<<"\nWpisz:\n'h', aby uzyc mikstury zdrowia\n'm', aby uzyc mikstury many\n'q', aby wrocic\n";
              cin>>temp;
            }
            while(temp!='h'&&temp!='m'&&temp!='q');
            switch(temp)
            { case 'h':
              { if(hero.poth>0)
                { hero.poth-=1;
                  int hptemp=hero.hp+30*poziom;
                  if(hptemp>hero.hpmax) hero.hp=hero.hpmax;
                  else hero.hp+=30*poziom;
                  goto menupotion;
                }
                else
                { system("cls");
                  cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                  sleep(2);
                  goto menupotion;
                }
              }
              case 'm':
              { if(hero.potm>0)
                { hero.potm-=1;
                  int manatemp=hero.mana+30*poziom;
                  if(manatemp>hero.manamax) hero.mana=hero.manamax;
                  else hero.mana+=30*poziom;
                  goto menupotion;
                }
                else
                { system("cls");
                  cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                  sleep(2);
                  goto menupotion;
                }
              }
              case 'q':
              { goto menuwalka;
              }
            }
          }
        }
      }
      if(wygran==1)
      { if(poziom==10)
        { system("cls");
          cout<<"\nWygrales!\n";
          sleep(2);
          goto menuglowne;
        }
        else
        { hero.exp+=potwor.lvl;
          if(hero.exp>=3*poziom)
          { hero.exp-=3*poziom;
            cout<<"\n\nZdobyles nowy poziom";
            sleep(2);
            poziom+=1;
            hero.jad=0;
            sklep=losujsklep(poziom);
            ilesklep=poziom;
            if(hero.klasa==0)
            { if(poziom==3)
              { do
                { system("cls");
                  cout<<"\nRozwin umiejetnosc. Wpisz:\n'k' dla kuli ognia\n'l' dla leczenia.\n";
                  cin>>temp;
                }
                while(temp!='k'&&temp!='l');
                switch(temp)
                { case 'k':
                    hero.ogien=1;
                    break;
                  case 'l':
                    hero.lecz=1;
                    break;
                }
              }
              if(poziom==5)
              { char znak;
                do
                { menuumiej:
                  system("cls");
                  cout<<"\nRozwin umiejetnosc. Wpisz:\n'k' dla kuli ognia\n'l' dla leczenia.\n";
                  cin>>znak;
                }
                while(temp!='k'&&temp!='l');
                if(temp=='k'&&hero.ogien==1) goto menuumiej;
                if(temp=='l'&&hero.lecz==1) goto menuumiej;
                switch(temp)
                { case 'k':
                    hero.ogien=1;
                    break;
                  case 'l':
                    hero.lecz=1;
                    break;
                }
              }
            }
            if(hero.klasa==1)
            { if(poziom==3)
              { do
                { system("cls");
                  cout<<"\nRozwin umiejetnosc. Wpisz:\n'f' dla furii\n't' dla zatruwania.\n";
                  cin>>temp;
                }
                while(temp!='f'&&temp!='t');
                switch(temp)
                { case 'f':
                    hero.furia=1;
                    break;
                  case 't':
                    hero.truj=1;
                    break;
                }
              }
              if(poziom==5)
              { char znak;
                do
                { menuumiej2:
                  system("cls");
                  cout<<"\nRozwin umiejetnosc. Wpisz:\n'f' dla furii\n't' dla zatruwania.\n";
                  cin>>znak;
                }
                while(temp!='f'&&temp!='t');
                if(temp=='f'&&hero.furia==1) goto menuumiej2;
                if(temp=='t'&&hero.truj==1) goto menuumiej2;
                switch(temp)
                { case 'f':
                    hero.furia=1;
                    break;
                  case 't':
                    hero.truj=1;
                    break;
                }
              }
            }
            do
            { system("cls");
              wyswietl(hero, poziom);
              cout<<"\nWybierz co chcesz rozwinac. Wpisz:\n'a' dla ataku\n'o' dla obrony\n'm' dla magii\n";
              cin>>temp;
            }
            while(temp!='a'&&temp!='o'&&temp!='m');
            switch(temp)
            { case 'a':
                hero.atak+=5*poziom;
                hero.hpmax+=25*poziom;
                hero.manamax+=5*poziom;
                break;
              case 'o':
                hero.obrona-=7*poziom;
                hero.hpmax+=25*poziom;
                hero.manamax+=5*poziom;
                break;
              case 'm':
                hero.hpmax+=13*poziom;
                hero.magia+=5*poziom;
                hero.manamax+=15*poziom;
                break;
            }
            hero.hp=hero.hpmax;
            hero.mana=hero.manamax;
          }
          if(ileitem==0)
          { glowa=tworz();
            losujitem(glowa,poziom);
            ileitem+=1;
            goto menuakcja;
          }
          else
          { przedmiot=tworz();
            losujitem(przedmiot,poziom);
            dodaj(glowa, przedmiot);
            ileitem+=1;
          }
        }
        goto menuakcja;
      }
      goto menuwalka;
    }
    case 'm':
    { menumiasto:
      do
      { system("cls");
        cout<<"\nWybierz co chcesz robic. Wpisz:\n'e', aby otworzyc ekwipunek\n's', aby pojsc do sklepu\n'q', aby wrocic\n";
        cin>>temp;
      }
      while(temp!='e'&&temp!='s'&&temp!='q');
      switch(temp)
      { case 'e':
        { menuekwipunek:
          system("cls");
          cout<<"\nOto Twoj ekwipunek: \n";
          drukuj(glowa);
          do
          { cout<<"\nWpisz:\n'p', aby zalozyc przedmiot\n't', aby zdjac przedmiot\n'm', aby uzyc miksury\n'q', aby wrocic\n";
            cin>>temp;
          }
          while(temp!='p'&&temp!='t'&&temp!='q'&&temp!='m');
          switch(temp)
          { case 'p':
            { char nrp;
              cout<<"\nPodaj nr przedmiotu, ktory chcesz zalozyc: \n";
              cin>>nrp;
              int nrp2=static_cast<int>(nrp)-48;
              if((ileitem==0)||(nrp2>ileitem))
              { system("cls");
                cout<<"\nNie ma takiego przedmiotu.\n";
                sleep(2);
                goto menuekwipunek;
              }
              else
              { przedmiot=tworz();
                przedmiot=znajdz(glowa, nrp2);
                if((przedmiot->uzywany==0)&&(zalozony==poziom))
                { system("cls");
                  cout<<"\nNie mozesz niesc wiecej przedmiotow.\n";
                  sleep(2);
                  goto menuekwipunek;
                }
                else
                { przedmiot->uzywany=1;
                  zalozony+=1;
                  aktualizuj(glowa,ileitem,hero);
                  goto menuekwipunek;
                }
              }
            }
            case 't':
            { char z;
              cout<<"\nPodaj nr przedmiotu, ktory chcesz zdjac: \n";
              cin>>z;
              int nrk=static_cast<int>(z)-48;
              if((ileitem==0)||(nrk>ileitem))
              { system("cls");
                cout<<"\nNie ma takiego przedmiotu.\n";
                sleep(2);
                goto menuekwipunek;
              }
              else
              { przedmiot=tworz();
                przedmiot=znajdz(glowa, nrk);
                if(przedmiot->uzywany==0) goto menuekwipunek;
                else
                { przedmiot->uzywany=0;
                  zalozony-=1;
                  aktualizuj(glowa,ileitem,hero);
                  goto menuekwipunek;
                }
              }
            }
            case 'm':
            { do
              { menupotionsklep:
                system("cls");
                wyswietl(hero, poziom);
                cout<<"\nWpisz:\n'h', aby uzyc mikstury zdrowia\n'm', aby uzyc mikstury many\n'q', aby wrocic\n";
                cin>>temp;
              }
              while(temp!='h'&&temp!='m'&&temp!='q');
              switch(temp)
              { case 'h':
                { if(hero.poth>0)
                  { hero.poth-=1;
                    int hptemp=hero.hp+30*poziom;
                    if(hptemp>hero.hpmax) hero.hp=hero.hpmax;
                    else hero.hp+=30*poziom;
                    goto menupotionsklep;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                    sleep(2);
                    goto menupotionsklep;
                  }
                }
                case 'm':
                { if(hero.potm>0)
                  { hero.potm-=1;
                    int manatemp=hero.mana+30*poziom;
                    if(manatemp>hero.manamax) hero.mana=hero.manamax;
                    else hero.mana+=30*poziom;
                    goto menupotionsklep;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie masz wystarczajacej liczby mikstur.\n";
                    sleep(2);
                    goto menupotionsklep;
                  }
                }
                case 'q':
                { goto menuekwipunek;
                }
              }
            }
            case 'q':
            { goto menumiasto;
            }
          }
        }
        case 's':
        { menusklep:
          char z2;
          do
          { system("cls");
            cout<<"\nTwoja ilośc zlota:     "<<hero.kasa<<"\n";
            cout<<"\nWpisz:\n's', aby sprzedac przedmiot\n'k', aby kupic przedmiot\n'm',aby kupic miksture\n'q' aby wrocic \n";
            cin>>z2;
          }
          while(z2!='s'&&z2!='k'&&z2!='q'&&z2!='m');
          switch(z2)
          { case 's':
            { char x;
              system("cls");
              cout<<"\nOto Twoj ekwipunek: \n";
              drukuj(glowa);
              cout<<"\nTwoja ilośc zlota:     "<<hero.kasa<<"\n";
              cout<<"\nPodaj nr przedmiotu, ktory chcesz sprzedac: \n";
              cin>>x;
              int nrs=static_cast<int>(x)-48;
              if((nrs<=0)||(nrs>ileitem))
              { system("cls");
                cout<<"\nNie ma takiego przedmiotu.\n";
                sleep(2);
                goto menusklep;
              }
              else
              { przedmiot=tworz();
                przedmiot=znajdz(glowa,nrs);
                hero.kasa+=przedmiot->wart;
                if(przedmiot->uzywany==1) zalozony-=1;
                usunelem(glowa,przedmiot);
                ileitem-=1;
                goto menusklep;
              }
            }
            case 'k':
            { menukupno:
              system("cls");
              cout<<"\nTwoja ilośc zlota:     "<<hero.kasa<<"\n";
              cout<<"\nPrzedmioty w sklepie: \n";
              drukuj(sklep);
              char y;
              cout<<"\nPodaj nr przedmiotu, ktory chcesz kupic, jesli nie chcesz nacisnij 0: \n";
              cin>>y;
              int nrk=static_cast<int>(y)-48;
              if((nrk<=0)||(nrk>ilesklep))
              { system("cls");
                if(nrk!=0)
                { cout<<"\nNie ma takiego przedmiotu.\n";
                  sleep(2);
                }
                goto menusklep;
              }
              else
              { przedmiot=tworz();
                przedmiot=znajdz(sklep,nrk);
                if(przedmiot->wart<=hero.kasa)
                { hero.kasa-=przedmiot->wart;
                  ilesklep-=1;
                  if(ileitem==0)
                  { usunelem(sklep,przedmiot);
                    przedmiot->next=NULL;
                    glowa=przedmiot;
                    ileitem+=1;
                  }
                  else
                  { usunelem(sklep,przedmiot);
                    przedmiot->next=NULL;
                    dodaj(glowa,przedmiot);
                    ileitem+=1;
                  }
                  goto menukupno;
                }
                else
                { system("cls");
                  cout<<"\nNie masz wystarczajacej ilosci pieniedzy.\n";
                  sleep(2);
                  goto menukupno;
                }
              }
            }
            case 'm':
            { do
              { menupotionkup:
                system("cls");
                wyswietl(hero, poziom);
                cout<<"\nWpisz:\n'h', aby kupic miksture zdrowia\n'm', aby kupic miksture many\n'q', aby wrocic\n";
                cin>>temp;
              }
              while(temp!='h'&&temp!='m'&&temp!='q');
              switch(temp)
              { case 'h':
                { if(hero.kasa-100*poziom>=0)
                  { hero.poth+=1;
                    hero.kasa-=100*poziom;
                    goto menupotionkup;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie masz wystarczajacej ilosci zlota\n";
                    sleep(2);
                    goto menupotionkup;
                  }
                }
                case 'm':
                { if(hero.kasa-100*poziom>=0)
                  { hero.potm+=1;
                    hero.kasa-=100*poziom;
                    goto menupotionkup;
                  }
                  else
                  { system("cls");
                    cout<<"\nNie masz wystarczajacej ilosci zlota\n";
                    sleep(2);
                    goto menupotionkup;
                  }
                }
                case 'q':
                { goto menusklep;
                }
              }
            }
            case 'q':
            { goto menumiasto;
            }
          }
        }
        case 'q':
        { goto menuakcja;
        }
      }
    }
    case 'z':
    { cout<<"\nPodaj nazwe pliku do zapisu:";
      string nazwa12;
      cin>>nazwa12;
      nazwa=nazwa12+".txt";
      zapisanie(hero,nazwa12);
      ofstream zapis(nazwa.c_str());
      zapis<<ileitem<<" "<<poziom<<" "<<ilesklep<<" "<<zalozony;
      zapis.close();
      zapiszekw(glowa,nazwa12);
      zapiszsklep(sklep,nazwa12);
      system("cls");
      goto menuglowne;
    }
    case 'q':
    { goto menuglowne;
    }
  }
}


