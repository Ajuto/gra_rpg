#ifndef ITEMY_H
#define ITEMY_H
#include <string>
using namespace std;
/**
 * \file Itemy.h
 * Plik nagłówkowy modułu Itemy
 */

/**  Definicja elementu jednokierunkowej dynamicznej listy przedmiotów.
 */
struct ITEM
{ ///wartość ataku
  int atak;
  ///wartość obrony
  int obrona;
  ///wartość magii
  int magia;
  ///wartość w złocie
  int wart;
  ///zmienna wskazująca czy przedmiot jest w użyciu
  bool uzywany;
  ///wskaźnik na następny element listy
  ITEM *next;
};


ITEM *losujsklep(int poziom);
void losujitem(ITEM *&akt, int poziom);
void zapiszekw(ITEM *adres, string nazwa1);
void wczytajekw(int ileitem, ITEM *&glowa, string nazwa3);
void usunliste(ITEM *&glowa);
void drukuj(ITEM *adres);
ITEM *tworz();
void dodaj(ITEM *pl, ITEM *e);
ITEM* znajdz(ITEM *glowa,int nr);
void usunelem(ITEM *&glowa, ITEM* e);
void zapiszsklep(ITEM *adres, string nazwa2);
void wczytajsklep(int ileitem, ITEM *&glowa, string nazwa6);

#endif // ITEMY_H
