#include "Itemy.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
using namespace std;

/**
 * \brief losujsklep
 * Funkcja losująca nowe przedmioty do sklepu
 * \param poziom poziom gracza
 * \return wskaźnik na pierwszy element listy
 */
ITEM *losujsklep(int poziom)
{ ITEM *akt,*glowa,*ogon;
  akt=NULL;
  glowa=NULL;
  for(int i=0;i<poziom;i++)
  { ogon=akt;
    akt=new ITEM;
    int rodzaj=rand()%2;
    if (rodzaj==0)
    { akt->atak=(1+rand()%4)*poziom;
      akt->magia=0;
    }
    else
    { akt->magia=(2+rand()%6)*poziom;
      akt->atak=0;
    }
    akt->obrona=(rand()%2)*poziom;
    akt->wart=1000*poziom;
    akt->uzywany=0;
    akt->next=NULL;
    if(ogon==NULL) glowa=akt;
    else ogon->next=akt;
  }
  return glowa;
}
/**
 * \brief losujitem
 * funkcja losująca przedmiot
 * \param akt wskaźnik gdzie dołączyć przedmiot
 * \param poziom poziom gracza
 */
void losujitem(ITEM *&akt, int poziom)
{ int rodzaj=rand()%2;
  if (rodzaj==0)
  { akt->atak=(1+rand()%4)*poziom;
    akt->magia=0;
  }
  else
  { akt->magia=(2+rand()%6)*poziom;
    akt->atak=0;
  }
  akt->obrona=(rand()%2)*poziom;
  akt->wart=500*poziom;
  akt->uzywany=0;
}
/**
 * \brief zapiszekw
 * Funkcja zapisująca ekwipunek do pliku
 * \param adres wskaźnik na początek listy
 */
void zapiszekw(ITEM *adres, string nazwa1)
{ nazwa1=nazwa1+"ekwipunek.txt";
  ofstream oplik(nazwa1.c_str());
  while(adres)
  { oplik<<adres->atak<<" "<<adres->obrona<<" "<<adres->magia<<" "<<adres->wart<<" "<<adres->uzywany<<"\n";
    adres=adres->next;
  }
  oplik.close();
}
/**
 * \brief zapiszsklep
 * Funkcja zapisująca przedmioty ze sklepu do pliku
 * \param adres wskaźnik na początek listy
 */
void zapiszsklep(ITEM *adres, string nazwa2)
{ nazwa2=nazwa2+"sklep.txt";
  ofstream oplik(nazwa2.c_str());
  while(adres)
  { oplik<<adres->atak<<" "<<adres->obrona<<" "<<adres->magia<<" "<<adres->wart<<" "<<adres->uzywany<<"\n";
    adres=adres->next;
  }
  oplik.close();
}
/**
 * \brief wczytajekw
 * Funkcja wczytująca ekwipunek
 * \param ileitem ile przedmiotów jest w ekwipunku
 * \param glowa wskaźnik na początek listy
 */
void wczytajekw(int ileitem, ITEM *&glowa, string nazwa3)
{ nazwa3=nazwa3+"ekwipunek.txt";
  ifstream iplik(nazwa3.c_str());
  ITEM *akt=NULL;
  ITEM *ogon=NULL;
  glowa=NULL;
  for(int i=0;i<ileitem;i++)
  { ogon=akt;
    akt=new ITEM;
    akt->next=NULL;
    iplik>>akt->atak>>akt->obrona>>akt->magia>>akt->wart>>akt->uzywany;
    if(ogon==NULL) glowa=akt;
    else ogon->next=akt;
  }
  ogon=akt;
  iplik.close();
}
/**
 * \brief wczytajsklep
 * Funkcja wczytująca przedmioty do sklepu
 * \param ileitem ile przedmiotów jest w sklepie
 * \param glowa wskaźnik na początek listy
 */
void wczytajsklep(int ileitem, ITEM *&glowa, string nazwa6)
{ nazwa6=nazwa6+"sklep.txt";
  ifstream iplik(nazwa6.c_str());
  ITEM *akt=NULL;
  ITEM *ogon=NULL;
  glowa=NULL;
  for(int i=0;i<ileitem;i++)
  { ogon=akt;
    akt=new ITEM;
    akt->next=NULL;
    iplik>>akt->atak>>akt->obrona>>akt->magia>>akt->wart>>akt->uzywany;
    if(ogon==NULL) glowa=akt;
    else ogon->next=akt;
  }
  ogon=akt;
  iplik.close();
}
/**
 * \brief usunliste
 * Funkcja usuwająca liste
 * \param glowa wskaźnik na początek listy
 */
void usunliste(ITEM *&glowa)
{ if(glowa==NULL) return;
  ITEM *tmp;
  if(glowa)
  { usunliste(glowa->next);
    tmp=glowa;
    glowa=NULL;
    delete tmp;
  }
}
/**
 * \brief drukuj
 * Funkcja wyświetlająca przedmioty
 * \param adres wskaźnik na początek listy
 */
void drukuj(ITEM *adres)
{ int i=1;
  while(adres)
  { cout<<"\n"<<i<<".";
    cout<<"\n    Atak:     "<<adres->atak;
    cout<<"\n    Obrona:   "<<adres->obrona;
    cout<<"\n    Magia:    "<<adres->magia;
    cout<<"\n    Wartosc:  "<<adres->wart;
    cout<<"\n    Zalozony? ";
    if(adres->uzywany==1) cout<<"Tak\n";
    if(adres->uzywany==0) cout<<"Nie\n";
    adres=adres->next;
    i+=1;
  }
}

/**
 * \brief tworz
 * Funkcja tworząca nowy elemnt listy jednokierunkiwej przedmiotóW
 * \return wskaźnik na nowy przedmiot
 */
ITEM* tworz()
{ ITEM* e= new ITEM;
  e->next=NULL;
  e->atak=0;
  e->uzywany=0;
  e->obrona=0;
  e->magia=0;
  e->wart=0;
  return e;
}

/**
 * \brief dodaj
 * Funkcja dodająca kolejny element do listy
 * \param pl wskaźnik na początek listy
 * \param e wskaźnik na element, który trzeba dodać
 */
void dodaj(ITEM *pl, ITEM *e)
{ ITEM *a=pl;
  while(a->next)
  { a=a->next;
  }
  a->next=e;
}
/**
 * \brief znajdz
 * Funkcja wyszukująca element w liście
 * \param glowa wskaźnik na początek listy
 * \param nr Numer elementu do wyszukania
 * \return wskaźnik na wyszukany element
 */
ITEM* znajdz(ITEM *glowa,int nr)
{ ITEM* a=glowa;
  for(int i=1;i<nr;i++)
  { a=a->next;
  }
  return a;
}
/**
 * \brief usunelem
 * funkcja usuwająca element z listy
 * \param glowa wskaźnik na początek listy
 * \param e wskaźnik na element do usunięcia
 */
void usunelem(ITEM *&glowa, ITEM* e)
{ ITEM* p=glowa;
  if(e==glowa)
  { glowa=e->next;
    return;
  }
  ITEM* a=glowa;
  ITEM* n=glowa->next;
  while(n)
  { if(a==e) break;
    p=a;
    a=n;
    n=n->next;
  }
  if(a==e)
  { p->next=a->next;
  }
}
