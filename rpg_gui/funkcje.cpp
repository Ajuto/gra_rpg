#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include "funkcje.h"
using namespace std;
/**
 * \brief loswrog
 * Funkcja losująca potwora
 * \param potwor struktura potwór
 * \param poziom poziom gracza
 */
void loswrog(POTWOR &potwor, int poziom)
{ potwor.lvl=1+rand()%poziom;
  potwor.atak=(3+rand()%(4))*potwor.lvl;
  potwor.obrona=(90+rand()%(10))-7*(potwor.lvl-1);
  potwor.truj=rand()%2;
  potwor.jad=0;
  potwor.hp=(50+rand()%20)*potwor.lvl;
  potwor.anty=(90+rand()%(10))-7*(potwor.lvl-1);
}
/**
 * \brief atakwrog
 * Funkcja symulująca atak potwora
 * \param potwor statystyki potwora
 * \param hero statystyki gracza
 * \param poziom poziom gracza
 */
void atakwrog(POTWOR potwor, POSTAC &hero, int poziom)
{ int iledmg=potwor.atak*double(hero.obrona/100);
  if(iledmg>3*poziom)
  { hero.hp-=potwor.atak*double(hero.obrona/100);
  }
  else hero.hp-=3*poziom;
  if(potwor.truj) hero.jad=1;
  if(hero.jad==1) hero.hp-=10*poziom;
}
/**
 * \brief atakhero
 * Funkcja symulująca atak gracza
 * \param potwor statystyki potwora
 * \param hero statystyki gracza
 * \param poziom poziom gracza
 */
void atakhero(POTWOR &potwor, POSTAC hero, int poziom)
{ int iledmg=hero.atak*double(potwor.obrona/100);
  if(iledmg>3*poziom) potwor.hp-=hero.atak*double(potwor.obrona/100);
  else potwor.hp-=3*poziom;
  if(hero.truj) potwor.jad=1;
  if(potwor.jad==1) potwor.hp-=10*poziom;
}
/**
 * \brief wyswietl
 * Funkcja wyświetlająca statystyki gracza
 * \param hero statystyki gracza
 * \param poziom poziom gracza
 */
void wyswietl(POSTAC hero, int poziom)
{ cout<<"\nTwoje statystyki: \n\n";
  cout<<"Poziom:"<<"             "<<poziom<<"\n";
  cout<<"Atak:"<<"               "<<hero.atak<<"\n";
  cout<<"Obrona:"<<"             "<<hero.obrona<<"\n";
  cout<<"Magia:"<<"              "<<hero.magia<<"\n";
  cout<<"Zdrowie:"<<"            "<<hero.hp<<"\n";
  cout<<"Mana:"<<"               "<<hero.mana<<"\n";
  cout<<"Doswiadczenie:"<<"      "<<hero.exp<<"\n";
  cout<<"Zatruty?";
  if(hero.jad==1) cout<<"            "<<"Tak\n";
  else cout<<"            "<<"Nie\n";
  cout<<"Zloto:"<<"              "<<hero.kasa<<"\n";
  cout<<"Mikstury zdrowia:"<<"   "<<hero.poth<<"\n";
  cout<<"Mikstury many:"<<"      "<<hero.potm<<"\n";
}
/**
 * \brief statywrog
 * Funkcja wyświetlająca statystyki potwora
 * \param potwor statystyki potwora
 */
void statywrog(POTWOR potwor)
{ cout<<"\nStatystyki wroga: \n\n";
  cout<<"Poziom:"<<"             "<<potwor.lvl<<"\n";
  cout<<"Atak:"<<"               "<<potwor.atak<<"\n";
  cout<<"Obrona:"<<"             "<<potwor.obrona<<"\n";
  cout<<"Odpornosc:"<<"          "<<potwor.anty<<"\n";
  cout<<"Zdrowie:"<<"            "<<potwor.hp<<"\n";
  cout<<"Zatruwa?";
  if(potwor.truj==1) cout<<"            "<<"Tak\n";
  else cout<<"            "<<"Nie\n";
}
/**
 * \brief tworzenie
 * Funcja tworząca postać
 * \param hero statystyki gracza
 */
void tworzenie(POSTAC &hero)
{ ofstream oplik;
  oplik.open("postac.txt");
  do
  { cout << "Wybierz klase postaci. Wpisz '0' dla maga lub '1' dla wojownika.";
    cin >> hero.klasa;
  } while(hero.klasa!=0&&hero.klasa!=1);
  hero.atak=10;
  hero.obrona=100;
  hero.magia=10;
  hero.truj=0;
  hero.jad=0;
  hero.exp=2;
  hero.hp=100;
  hero.hpmax=100;
  hero.mana=100;
  hero.manamax=100;
  hero.lecz=0;
  hero.ogien=0;
  hero.kasa=10000;
  hero.poth=3;
  hero.potm=2;
  hero.furia=0;
  oplik<<hero.klasa<<" "<<hero.atak<<" "<<hero.obrona<<" "<<hero.magia<<" "<<hero.truj<<" "<<hero.jad<<" "<<hero.exp<<" "<<hero.hp<<hero.hpmax<<" "<<hero.mana<<" "<<hero.lecz<<" "<<hero.ogien<<" "<<hero.kasa<<" "<<hero.poth<<" "<<hero.potm<<" "<<hero.manamax<<" "<<hero.furia;
  oplik.close();
}
/**
 * \brief wczytanie
 * Funcja wczytująca statystyki gracza z pliku
 * \param hero statystyki gracza
 */
void wczytanie(POSTAC &hero)
{ ifstream iplik("postac.txt");
  iplik>>hero.klasa>>hero.atak>>hero.obrona>>hero.magia>>hero.truj>>hero.jad>>hero.exp>>hero.hp>>hero.hpmax>>hero.mana>>hero.lecz>>hero.ogien>>hero.kasa>>hero.poth>>hero.potm>>hero.manamax>>hero.furia;
  iplik.close();
}
/**
 * \brief zapisanie
 *Funkcja zapisująca statystyki gracza do pliku
 * \param hero statystyki gracza
 */
void zapisanie(POSTAC &hero)
{ ofstream oplik;
  oplik.open("postac.txt");
  oplik<<hero.klasa<<" "<<hero.atak<<" "<<hero.obrona<<" "<<hero.magia<<" "<<hero.truj<<" "<<hero.jad<<" "<<hero.exp<<" "<<hero.hp<<" "<<hero.hpmax<<" "<<hero.mana<<" "<<hero.lecz<<" "<<hero.ogien<<" "<<hero.kasa<<" "<<hero.poth<<" "<<hero.potm<<" "<<hero.manamax<<" "<<hero.furia;
  oplik.close();
}
