#-------------------------------------------------
#
# Project created by QtCreator 2014-05-29T19:28:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rpg_gui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Itemy.cpp \
    funkcje.cpp

HEADERS  += mainwindow.h \
    Itemy.h \
    funkcje.h

FORMS    += mainwindow.ui
